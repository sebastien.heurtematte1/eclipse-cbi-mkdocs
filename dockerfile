FROM python:slim

WORKDIR /app/mkdocs
COPY . /app/mkdocs

# RUN apt update && apt install python3-pip git bash -y

# SHELL ["/bin/bash", "-c"]

USER root

RUN apt update && apt install git -y
RUN rm /dev/null
RUN git config --global user.name "heurtemattes" && git config --global user.email "sebastien.heurtematte@eclipse-foundation.org"
RUN pip install -r requirements.txt
RUN mkdocs build --verbose